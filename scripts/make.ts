import * as path from 'path'
import { promises as fs } from 'fs'

const __root = path.join(__dirname, '../statics')
const main = async () => {
	const dir = path.join(__root, 'parts')
	await fs.mkdir(dir)
	for (let i = 1; i <= 15; ++i) {
		const pagename = `list${i}.html`
		await fs.writeFile(path.join(dir, pagename), html(i))
	}
}

const html = (page: number) => `<ul>
${[1, 2, 3, 4, 5]
	.map(i => 5 * (page - 1) + i)
	.map(
		id => `	<li data-controller="listitem" data-listitem-id-value="${id}"></li>`,
	)
	.join('\n')}
</ul>
`

main().catch(x => {
	console.error(x)
	process.exit(1)
})
