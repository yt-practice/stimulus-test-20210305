import '@stimulus/polyfills'
import { Application } from 'stimulus'
import { Controller } from 'stimulus'
import Hello from './controllers/hello'
import List from './controllers/list'
import Listitem from './controllers/listitem'

export const main = async () => {
	const app = Application.start()
	app.register('hello', Hello)
	app.register('list', List)
	app.register('listitem', Listitem)
	app.register(
		'piyo',
		class extends Controller {
			connect() {
				this.element.textContent = 'piyo!'
				sleep(3000).then(() => {
					this.element.innerHTML = `<div data-controller="fuga"></div>`
				})
			}
		},
	)
	await sleep(1500)
	app.register(
		'fuga',
		class extends Controller {
			connect() {
				this.element.textContent = 'fuga!'
			}
		},
	)
}

const sleep = (s: number) => new Promise<void>(r => setTimeout(r, s))

main().catch(x => {
	console.log('# something happens.')
	console.error(x)
	if ('undefined' === typeof process) return
	process.exit(1)
})
