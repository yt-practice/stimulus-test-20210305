import { Controller } from 'stimulus'

export class Listitem extends Controller {
	static values = { id: Number }
	idValue!: number
	_handle!: () => void
	initialize() {
		this._handle = () => {
			console.log(this.idValue)
		}
	}
	connect() {
		this.element.innerHTML = `<div>item ${this.idValue}</div>`
		this.element.addEventListener('click', this._handle)
	}
	disconnect() {
		this.element.removeEventListener('click', this._handle)
	}
}

export default Listitem
