import { Controller } from 'stimulus'

export class List extends Controller {
	static targets = ['items', 'pagenum']
	itemsTarget!: HTMLElement
	hasPagenumTarget!: boolean
	pagenumTarget!: HTMLElement
	_page!: number
	initialize() {
		this._page = 1
	}
	connect() {
		this.loadPage()
	}
	next() {
		this._page++
		this.loadPage()
	}
	prev() {
		this._page--
		this.loadPage()
	}
	async loadPage() {
		const pagenum = this._page
		const parts = await fetch(`./parts/list${pagenum}.html`).then(r =>
			r.ok ? r.text() : 'no items!',
		)
		if (this.hasPagenumTarget) this.pagenumTarget.textContent = String(pagenum)
		this.itemsTarget.innerHTML = parts
	}
}

export default List
