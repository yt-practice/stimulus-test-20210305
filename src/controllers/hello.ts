import { Controller } from 'stimulus'

export class Hello extends Controller {
	static targets = ['output']
	outputTarget!: HTMLElement
	greet(e: { target: { value: string } }) {
		const element = e.target
		const name = element.value
		this.outputTarget.textContent = `Hello, ${name}!`
	}
}

export default Hello
